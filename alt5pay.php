<?php
/*
Plugin Name: Alt5pay
Plugin URI: https://gitlab.com/franciscoblancojn/alt5pay
Description: Integración de woocommerce con los servicios de envío de Alt5pay.
Author: FranciscoBlanco
Version: 2.0.46
Author URI: https://franciscoblanco.vercel.app/
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/alt5pay',
	__FILE__,
	'alt5pay'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');


define("ALT5PAY_DIR",plugin_dir_path( __FILE__ ));
define("ALT5PAY_URL",plugin_dir_url( __FILE__ ));

require_once  ALT5PAY_DIR . 'src/_index.php';