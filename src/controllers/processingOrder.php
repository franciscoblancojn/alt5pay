<?php
function ALT5PAY_processingOrder($order_id,$data)
{
    $order = new WC_Order($order_id);
    $order->update_status('pre-processing');
    update_post_meta($order_id,'ALT5PAY_processingOrder',json_encode($data));
    ALT5PAY_sendEmail_Pre_Procesando($order_id);
}