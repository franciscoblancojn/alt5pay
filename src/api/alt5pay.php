<?php

class ApiAlt5pay {
    public function __construct($settings = null)
    {
        if($settings == null){
            $Alt5play_Gateway = new WC_Alt5play_Gateway();
            $settings = array(
                'testmode'=> $Alt5play_Gateway->get_option('testmode'),
                'merchant_id'=> $Alt5play_Gateway->get_option('merchant_id'),
                'public_key'=> $Alt5play_Gateway->get_option('public_key'),
                'secrect_key'=> $Alt5play_Gateway->get_option('secrect_key'),
            );
        }
        $this->settings = $settings;
    }
    private function url($cripto)
    {
        $mode = $this->settings['testmode'] == 'yes' ? "digitalpaydev":"alt5pay";
        return "https://api.".$mode.".com:5000/usr/wallet/".$cripto."/create";
    }

    private function auth($order_id)
    {
        $merchant_id = $this->settings["merchant_id"];
        $public_key = $this->settings["public_key"];
        $secrect_key = $this->settings["secrect_key"];
        $timestamp = explode(" ",microtime())[1];
        $nonce = random_int(100000,999999);
        $url=ALT5PAY_URL."src/routes/processing.php?order_id=".$order_id;
        $bodyString ='ref_id=order_'.$order_id.'&timestamp='.$timestamp.'&nonce='.$nonce."&url=".$url;
        
        $hash = hash_hmac('sha512',$bodyString,$secrect_key);
        $auth = base64_encode($public_key.":".$hash);
        
        $json = array(
            "ref_id"=>"order_".$order_id,
            "timestamp"=>$timestamp,
            "nonce"=>$nonce,
            "url"=>$url,
        );
        return array(
            "json"=>$json,
            "auth"=>$auth
        );
    }

    public function request($url,$json,$auth)
    {
        $curl = curl_init();
        
        $header = array(
            'apikey: '.$this->settings["public_key"],
            'merchant_id: '.$this->settings["merchant_id"],
            'authentication: '.$auth,
            'Content-Type: application/json'
        );

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($json),
          CURLOPT_HTTPHEADER => $header,
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return json_decode($response,true);
        
    }
    public function createPayment($order_id)
    {
        $cripto = get_post_meta($order_id,'ALT5PAY_cripto',true);

        $url = $this->url($cripto);
        $auth = $this->auth($order_id);


        $result = $this->request(
            $url,
            $auth["json"],
            $auth["auth"],
        );
        return $result;
    }

}
