<?php

class ApiLivecoinwatch {
    private $URL = "https://api.livecoinwatch.com";

    public function __construct()
    {
    }

    private function request($url,$json)
    {
        $curl = curl_init();
        
        $header = array(
            'x-api-key: '.get_option("ALT5PAY_livecoinwatch_apikey"),
            'Content-Type: application/json'
        );

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->URL.$url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($json),
          CURLOPT_HTTPHEADER => $header,
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return json_decode($response,true);
        
    }
    public function getCripto($cripto)
    {
        return $this->request(
            "/coins/single",
            array(
                "currency" => "USD",
                "code" =>  $cripto,
                "meta" => true,
            )
        );
    }
    public function getAllCripto()
    {
        return $this->request(
            "/coins/list",
            array(
                "currency" => "USD",
                "sort" => "rank",
                "order" => "ascending",
                "offset" => 0,
                "limit" => 50,
            )
        );
    }
}
