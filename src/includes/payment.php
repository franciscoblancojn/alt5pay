<?php


/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'alt5play_init_gateway_class' );
function alt5play_init_gateway_class() {

	class WC_Alt5play_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {
            $this->id = 'alt5play';
            $this->icon = ALT5PAY_URL."src/img/criptos.png"; 
            $this->has_fields = true;
            $this->method_title = 'Alt5play';
            $this->method_description = 'Description of Alt5play payment gateway';
            
            $this->supports = array(
                'products'
            );
        
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
        
            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        
            // We need custom JavaScript to obtain a token
            add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
            
            // You can also register a webhook here
            // add_action( 'woocommerce_api_{webhook name}', array( $this, 'webhook' ) );
 		}

		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable Alt5play',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'Titulo de Alt5play',
                    'default'     => 'Alt5play',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'Description de Alt5play',
                    'default'     => 'Alt5play, pagos con criptomonedas',
                ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Modo de Pruebas Alt5play',
                    'default'     => 'no',
                    'desc_tip'    => true,
                ),
                'public_key' => array(
                    'title'       => 'Public Key',
                    'type'        => 'text',
                    'description' => 'Public key de Alt5play, obtener en https://dashboard.alt5pay.com/#/settings/api',
                    'default'     => '',
                    'desc_tip'    => true,
                ),
                'secrect_key' => array(
                    'title'       => 'Secrect Key',
                    'type'        => 'password',
                    'description' => 'Secrct key de Alt5play, obtener en https://dashboard.alt5pay.com/#/settings/api',
                    'default'     => '',
                    'desc_tip'    => true,
                ),
                'merchant_id' => array(
                    'title'       => 'Merchant ID',
                    'type'        => 'text',
                    'description' => 'Merchant ID de Alt5play',
                    'default'     => '',
                    'desc_tip'    => true,
                ),
            );
	
	 	}

         /**
          * You will need it if you want your custom credit card form, Step 4 is about it
          */
         public function payment_fields() {
            $pricesCripto = getPricesForCriptos(0);
            ?>
            <script>
                pricesCripto = <?=json_encode($pricesCripto)?>;
            </script>
            <style>
                .payment_box.payment_method_alt5play{
                    display:none !important;
                }
            </style>
            <?php
         }

		/*
		 * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
		 */

        public function payment_scripts() {
            if ( ! is_cart() && ! is_checkout() && ! isset( $_GET['pay_for_order'] ) ) {
                return;
            }
            if ( 'no' === $this->enabled ) {
                return;
            }
            wp_enqueue_script( 'ALT5PAY_payment_scripts', ALT5PAY_URL.'src/js/showPriceCriptoCheckout.js' );
        
        }
		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            global $woocommerce;

            $woocommerce->cart->empty_cart();
            // Redirect to the thank you page
            return array(
                'result' => 'success',
                'redirect' => ALT5PAY_URL."src/routes/pay.php?order_id=".$order_id

            );
					
	 	}

		/*
		 * In case you need a webhook, like PayPal IPN etc
		 */
		public function webhook() {

					
	 	}
 	}
}

add_filter( 'woocommerce_payment_gateways', 'alt5play_add_gateway_class' );
function alt5play_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Alt5play_Gateway';
	return $gateways;
}