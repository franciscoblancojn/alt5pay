<?php

function getPricesForCriptos($price)
{
    $api = new ApiLivecoinwatch();
    $allCriptos = $api->getAllCripto();
    $allCriptos = array_filter($allCriptos,function($ele)
        {
            return in_array(strtoupper($ele["code"]),[
                "BTC","BCH","ETH","LTC","USDT"
            ]);
        }
    );
    $rates = [];
    foreach ($allCriptos as $key => $value) {
        $rates[$value["code"]] = array(
            "rate" => $value["rate"],
            "cripto" => floatval($price) / floatval($value["rate"]),
            "code" => $value["code"],
        );
    }
    return $rates;
}
function getPricesForCripto($cripto,$price){
    if($cripto == "erc20/usdt"){
        $cripto = "USDT";
    }
    $cripto = strtoupper($cripto);
    $allCriptos = getPricesForCriptos($price);
    return $allCriptos[$cripto];
}

function shortcode_getPricesForCripto()
{
    echo "<pre>";
    var_dump(getPricesForCripto("ltc",50));
    echo "</pre>";
}
add_shortcode('shortcode_getPricesForCripto', 'shortcode_getPricesForCripto');