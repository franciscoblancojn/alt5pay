<?php
function ALT5PAY_sendEmail($subject,$message,$email){
    add_filter('wp_mail_content_type', function( $content_type ) {
        return 'text/html';
    });
    wp_mail($email, $subject, $message);
}
function ALT5PAY_sendEmail_Pre_Procesando($order_id)
{
    $order = wc_get_order( $order_id );
    $subject = "Procesando Pedido";
    $user_name = $user->user_login;
    $email = $order->get_billing_email();
    ob_start();
    require_once ALT5PAY_DIR . "src/template/email/pre-processing.php";
    $message = ob_get_clean();
    ALT5PAY_sendEmail($subject,$message,$email);
}