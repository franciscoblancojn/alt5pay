<?php
 
function ALT5PAY_register_cron_validatePayment() {
    if (! wp_next_scheduled ( 'ALT5PAY_cron_validatePayment' )) {
        wp_schedule_event(time(), 'hourly', 'ALT5PAY_cron_validatePayment');
    }
}
 
if (! wp_next_scheduled ( 'ALT5PAY_cron_validatePayment' )) {
    wp_schedule_event(time(), 'hourly', 'ALT5PAY_cron_validatePayment');
}
register_activation_hook(__FILE__, 'ALT5PAY_register_cron_validatePayment');
 
function ALT5PAY_cron_validatePayment() {
    $orders_ids = wc_get_orders(array(
        'status' => ['wc-pending'],  
        'return' => 'ids',  
    ));
    $dateNowInt = strtotime(date('c'));
    for ($i=0; $i < count($orders_ids); $i++) { 
        $order_id = $orders_ids[$i];  
        $order = wc_get_order( $order_id );
        $dateModifyInt = strtotime("+ 3 hour",strtotime($order->get_date_modified()));
        if($dateNowInt >= $dateModifyInt){
            $order->update_status( 'cancelled' );
        }
    }
}
add_shortcode('ALT5PAY_cron_validatePayment', 'ALT5PAY_cron_validatePayment');
add_action('ALT5PAY_cron_validatePayment', 'ALT5PAY_cron_validatePayment');