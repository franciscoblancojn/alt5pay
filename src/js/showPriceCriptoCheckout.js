const loadTdPriceCripto = () => {
  const tfoot = document.querySelector("#order_review tfoot");
  tfoot.innerHTML += `
    <tr class="order-total">
        <th>Total Price Cripto</th>
        <td>
            <strong>
                <span class="">
                    <bdi>
                        <span id="priceCriptoSpan"></span>
                    </bdi>
                </span>
            </strong>
        </td>
    </tr>
    `;
};

const loadPriceCripto = () => {
  const priceText = document.querySelector(
    ".order-total .woocommerce-Price-amount.amount"
  );
  const price = priceText.innerHTML.replace(/[^\d\.]/g, "");
  const cripto = document.getElementById("cripto");
  const priceCriptoSpan = document.getElementById("priceCriptoSpan");
  var criptoSelect = cripto.value;
  if (criptoSelect == "erc20/usdt") {
    criptoSelect = "USDT";
  }
  criptoSelect = criptoSelect.toUpperCase();

  const priceCripto = pricesCripto[criptoSelect].rate;
  priceCriptoSpan.innerHTML =
    (parseFloat(price) / priceCripto).toFixed(10) + " " + criptoSelect;
};

const loadShowPriceCriptoCheckout = () => {
  console.log("loadShowPriceCriptoCheckout");
  const cripto = document.getElementById("cripto");
  cripto.addEventListener("change", (e) => {
    loadPriceCripto();
  });
  loadPriceCripto();
};

window.addEventListener("load", () => {
  loadTdPriceCripto();
  loadShowPriceCriptoCheckout();
  jQuery(document.body).on("updated_checkout", function () {
    loadTdPriceCripto();
    loadShowPriceCriptoCheckout();
  });
});
