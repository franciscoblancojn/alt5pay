const loopTimer = (time, update) => {
    time.second--
    if(time.second < 0){
        time.minute -= 1
        time.second = 59
        if(time.minute < 0){
            time.hour -= 1
            time.minute = 59
            if(time.hour < 0){
                time = {
                    hour: 0,
                    minute: 0,
                    second: 0,
                };
            }
        }
    }
    if(update(time)){
        setTimeout(()=>{
            loopTimer(time, update)
        },1000)
    }

};

const initTimer = () => {
  const timer = document.getElementById("timer");

  var time = {
    hour: 2,
    minute: 0,
    second: 0,
  };
  loopTimer(time, (e) => {
    const hour = e.hour > 0 ? `${e.hour} horas` : "";
    const minute = e.minute > 0 ? `${e.minute} mins` : "";
    const second = e.second > 0 ? `${e.second} segundos` : "";

    if( e.hour < 1 && e.minute < 1 && e.second < 1){
        timer.innerHTML = `Expiro`;
        return false
    }

    timer.innerHTML = `Expira en ${hour} ${minute} ${second}`;
    return true
  });
};

window.addEventListener("load", () => {
  initTimer();
});
