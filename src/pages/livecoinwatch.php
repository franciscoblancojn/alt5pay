<?php
function ALT5PAY_livecoinwatch_create_menu() {

	//create new top-level menu
	add_menu_page('Livecoinwatch', 'Livecoinwatch', 'administrator', __FILE__, 'ALT5PAY_livecoinwatch_settings_page'  );

	//call register settings function
	add_action( 'admin_init', 'register_ALT5PAY_livecoinwatch_settings' );
}
add_action('admin_menu', 'ALT5PAY_livecoinwatch_create_menu');


function register_ALT5PAY_livecoinwatch_settings() {
	//register our settings
	register_setting( 'ALT5PAY_livecoinwatch-settings-group', 'new_option_name' );
	register_setting( 'ALT5PAY_livecoinwatch-settings-group', 'some_other_option' );
	register_setting( 'ALT5PAY_livecoinwatch-settings-group', 'option_etc' );
}

function ALT5PAY_livecoinwatch_settings_page() {
    if($_POST){
        update_option("ALT5PAY_livecoinwatch_apikey",$_POST["apikey"]);
    }
    ?>
    <h1>
        Livecoinwatch
    </h1>
    <form method="post">
        <label for="">
            Api Key
            <br>
            <input type="text" name="apikey" id="apikey" value="<?=get_option("ALT5PAY_livecoinwatch_apikey")?>">
        </label>
        <br>
        <br>
        <button class="button action">Save</button>
    </form>
    <?php 
}