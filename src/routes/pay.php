<?php

require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

get_header();

$order_id = $_GET["order_id"];
$order = wc_get_order( $order_id );

$cripto = get_post_meta($order_id,'ALT5PAY_cripto',true);
$api = new ApiAlt5pay();

$result = $api->createPayment($order_id);


echo "<div class='ApiAlt5pay_pay'>";
if($result["status"]=="success"){
    $qr = $result["data"]["address"];
    switch ($cripto) {
        case 'btc':
            $qr = "bitcoin:".$result["data"]["address"];
            break;
    }
    $urlCode = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=".$qr;
    ?>
        <h1 class="title">Realizar Pago</h1>
            <div class="subContent">
                <a class="text-info-1" href="#">
                    Si no sabes como hacer, esto  te recomendamos leer esta Guía.
                </a>
                <p class="text-info-2">
                    Para  realizar tu pago recuerda que debes depositar el dinero en el siguiente <strong>monedero</strong> de criptomonedas.
                </p>
            </div>
        <div class="col1 col50">
            <div class="space" style="width: 100%;">
               
                <div class="flex flex-between pass-margin ">
                    <div class="number">1.</div>
                        <div class="content-steps">
                            <h2 class="title-steps-1">Moneda</h2>
                                <img class="imgcripto" src="<?=ALT5PAY_URL?>src/img/<?=str_replace("/","",$cripto)?>.png">
                            <h2 class="title-steps-1">
                                <?php
                                    $money = getPricesForCripto($cripto,$order->get_total());
                                    echo number_format($money["cripto"],10)." ".$money["code"];
                                ?>    
                            
                            </h2>
                        </div>
                    <div class="number void" style="opacity:0;">1.</div>
                </div>
                <p class="text-steps-1">
                    Ten presente que el numero de la billetera es único para cadatipo de moneda, verifica bien la RED y su numero.
                </p>
                <div>
                    <iframe class="codeQr" src="<?=$urlCode?>" frameborder="0"></iframe>
                </div>
                <h2 class="title-money">
                    Monedero
                </h2>
                <div id="copyContent" class="contentInput">
                    <input id="address" type="text" value="<?=$result["data"]["address"]?>" disabled>
                    <input id="addressText" type="text" value="<?=$result["data"]["address"]?>" style="position: fixed;top: 100%;left: 100%;transform: scale(0);">
                    <svg  width="22" height="27" viewBox="0 0 22 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.5659 4.73682H2.58292C2.04249 4.73708 1.52427 4.95188 1.14213 5.33402C0.759986 5.71616 0.545187 6.23439 0.544922 6.77482V23.9688C0.545187 24.5092 0.759986 25.0275 1.14213 25.4096C1.52427 25.7917 2.04249 26.0065 2.58292 26.0068H14.5659C15.1064 26.0065 15.6246 25.7917 16.0067 25.4096C16.3889 25.0275 16.6037 24.5092 16.6039 23.9688V6.77482C16.6018 6.23496 16.3864 5.7178 16.0047 5.33606C15.6229 4.95432 15.1058 4.73892 14.5659 4.73682V4.73682ZM15.1729 23.9628C15.1727 24.125 15.1081 24.2806 14.9934 24.3953C14.8787 24.51 14.7232 24.5745 14.5609 24.5748H2.57692C2.41469 24.5745 2.25918 24.51 2.14446 24.3953C2.02975 24.2806 1.96518 24.125 1.96492 23.9628V6.77482C1.96518 6.61259 2.02975 6.45707 2.14446 6.34235C2.25918 6.22764 2.41469 6.16308 2.57692 6.16281H14.5609C14.7232 6.16308 14.8787 6.22764 14.9934 6.34235C15.1081 6.45707 15.1727 6.61259 15.1729 6.77482V23.9628Z" fill="#9D9D9D"/>
                        <path d="M19.1475 0.229004H7.16449C6.62406 0.229269 6.10583 0.444071 5.72369 0.826213C5.34155 1.20835 5.12674 1.72657 5.12648 2.267C5.12289 2.36282 5.13866 2.45837 5.17285 2.54795C5.20704 2.63752 5.25895 2.71928 5.32547 2.78834C5.39198 2.85739 5.47173 2.91232 5.55996 2.94984C5.6482 2.98736 5.74311 3.0067 5.83899 3.0067C5.93487 3.0067 6.02976 2.98736 6.11799 2.94984C6.20623 2.91232 6.28599 2.85739 6.3525 2.78834C6.41902 2.71928 6.47092 2.63752 6.50511 2.54795C6.5393 2.45837 6.55507 2.36282 6.55148 2.267C6.55174 2.10477 6.61631 1.94926 6.73103 1.83455C6.84574 1.71983 7.00125 1.65527 7.16348 1.655H19.1465C19.3087 1.65527 19.4642 1.71983 19.5789 1.83455C19.6936 1.94926 19.7582 2.10477 19.7585 2.267V19.46C19.7582 19.6222 19.6936 19.7777 19.5789 19.8925C19.4642 20.0072 19.3087 20.0717 19.1465 20.072C19.0507 20.0684 18.9551 20.0842 18.8655 20.1184C18.776 20.1526 18.6942 20.2045 18.6251 20.271C18.5561 20.3375 18.5012 20.4173 18.4636 20.5055C18.4261 20.5937 18.4068 20.6886 18.4068 20.7845C18.4068 20.8804 18.4261 20.9753 18.4636 21.0635C18.5012 21.1517 18.5561 21.2315 18.6251 21.298C18.6942 21.3645 18.776 21.4164 18.8655 21.4506C18.9551 21.4848 19.0507 21.5006 19.1465 21.497C19.6869 21.4967 20.2051 21.2819 20.5873 20.8998C20.9694 20.5177 21.1842 19.9994 21.1845 19.459V2.267C21.1842 1.72675 20.9696 1.20868 20.5876 0.826566C20.2057 0.444453 19.6877 0.229534 19.1475 0.229004V0.229004Z" fill="#9D9D9D"/>
                    </svg>
                    <input id="copy" type="button" class="current" value="Copiar">

                    <script type="text/javascript">
                        var button = document.getElementById("copyContent"),
                        address = document.getElementById("addressText");
                    
                        button.addEventListener("click", function(event) {
                            event.preventDefault();
                            address.select();
                            document.execCommand("copy");
                        });
                    </script></ul>
                </div>
                <h3 id="timer">
                </h3>
                <p class="text-info-end">
                    Te recomendamos no cerrar esta pagina o pestaña en tu navegador.
                </p>
            </div>
        </div>
        <div class="col2 col50">
            <div class="title-steps-2" style="width: 100%;">
                <div class="flex direcc">
                    <div class="number">2.</div>
                    <div class="content-imgInfo2" style="max-width:calc(100% - 40px)">
                        <img class="imgemail" src="<?=ALT5PAY_URL?>src/img/email.png">
                        <p class="text-steps-2">Una vez que el sistema haya confirmado tu pago, recibirás un correo electronico confirmando tu compra</p>
                    </div>
                    <div></div>
                </div>
                <div class="flex direcc">
                    <div class="number">3.</div>
                    <div class="content-imgInfo3" style="max-width:calc(100% - 40px)">
                        <img class="imgdiablo" src="<?=ALT5PAY_URL?>src/img/diablo.png">
                        <p class="text-steps-3
                        ">En un periodo de 24 Horas, la modelo que seleccionaste se contactara contigo por chat y procederá a realizar tu show o si compraste algún contenido podrás acceder a esta directamente en la sección de MI CUENTA.</p>
                    </div>
                    <div></div>
                </div>
            </div>
        </div>
        <!-- <h2><strong><?=$result["data"]["address"]?></strong></h2>
        <h1>Expires</h1>
        <h2><strong><?=$result["data"]["expires"]?></strong></h2>
        <h1>Coin</h1>
        <h2><strong><?=$result["data"]["coin"]?></strong></h2> -->
        <link rel="stylesheet" href="<?=ALT5PAY_URL?>src/css/pay.css">
        <script src="<?=ALT5PAY_URL?>src/js/pay.js"></script>
    <?php
}else{
    echo "<h1>Error</h1>";
    echo "<pre>";
    var_dump($result);
    echo "</pre>";
}
echo "</div>";


get_footer();