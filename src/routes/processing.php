<?php

require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}
$data = $_POST;
$order_id = $_GET["order_id"];

if(ALT5PAY_valiteOrder($order_id)){
    ALT5PAY_processingOrder($order_id,$data);
}
