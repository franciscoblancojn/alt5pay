<?php

function ALT5PAY_btn_whatsapp(){
    if( current_user_can('administrator') ) {
        $order_id = $_GET["post"];
        $order = wc_get_order( $order_id );
        if($order->get_status() == "processing" ){
            $orderTOTAL = $order->get_total();
            $wa_number_client = get_post_meta( $order_id, '_wa_number', true );
            foreach ( $order->get_items() as $item_id => $item ) {
                    $product_id = $item->get_product_id();
                    $product = wc_get_product( $product_id );
                    $produt_name = $product->get_name();
                    $parent_id  = $product->get_parent_id();
                    if($parent_id != 0){
                        $product_id = $parent_id;
                    }
                    $modelo_relacinadas = get_post_meta($product_id,'relation_8e68fe393684baec9343dee47bca3fe2');
                    for ($i=0; $i < count($modelo_relacinadas); $i++) {
                        $wa_number_model = get_post_meta($modelo_relacinadas[$i],'whatsapp',true);
                        $text = "Haz recibido una solicitud éxitosa, por $".$orderTOTAL.", número de orden {$order_id}, prouducto $produt_name, por favor agrega este número de WhatsApp para proceder a realizar el show {$wa_number_client}";
                        $url = "https://api.whatsapp.com/send?phone=$wa_number_model&text=$text";
                        ?>
                        <div class="edit">
                            <a class="button" href="<?=$url?>" target="_blank">
                                Avisar a Modelo Por Whatsapp (<?=$produt_name?>)
                            </a>
                        </div>
                        <?php
                    }
            }
                
        }
    }
}
add_action( 'woocommerce_after_order_itemmeta', 'ALT5PAY_btn_whatsapp' );