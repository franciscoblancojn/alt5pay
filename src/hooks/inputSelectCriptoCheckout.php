<?php


add_action('woocommerce_before_order_notes', 'ALT5PAY_add_select_checkout_field');

function ALT5PAY_add_select_checkout_field( $checkout ) {
    woocommerce_form_field( 'cripto', array(
            'type'          => 'select',
            'class'         => array( 'cripto-drop' ),
            'label'         => __( 'Cripto options' ),
            'options'       => array(
                'btc'             => "BTC",
                'bch'             => "BCH",
                'eth'             => "ETH",
                'ltc'             => "LTC",
                // 'doge'            => "DODGE",
                'erc20/usdt'      => "USDT",
            )
        ),
        $checkout->get_value( 'cripto' )
    );
}

add_action( 'woocommerce_checkout_create_order', 'ALT5PAY_order_custom_meta_data', 10, 2 );
function ALT5PAY_order_custom_meta_data( $order, $data ) {
    if ( isset($_POST['cripto']) ){
        update_post_meta($order->get_id(),'ALT5PAY_cripto',$_POST['cripto']);
        $order->update_meta_data('ALT5PAY_cripto',  $_POST['cripto']  );
    }
}