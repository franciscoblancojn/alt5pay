<?php

function register_pre_processing_order_status() {
    register_post_status( 'wc-pre-processing', array(
        'label'                     => 'Pre Processing',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Pre Processing (%s)', 'Pre Processing (%s)' )
    ) );
}
add_action( 'init', 'register_pre_processing_order_status' );


function add_pre_processing_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-pre-processing'] = 'Pre Processing';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_pre_processing_to_order_statuses' );