<?php

require_once  ALT5PAY_DIR . 'src/api/_index.php';
require_once  ALT5PAY_DIR . 'src/controllers/_index.php';
require_once  ALT5PAY_DIR . 'src/hooks/_index.php';
require_once  ALT5PAY_DIR . 'src/includes/_index.php';
require_once  ALT5PAY_DIR . 'src/middlewares/_index.php';
require_once  ALT5PAY_DIR . 'src/cron/_index.php';
require_once  ALT5PAY_DIR . 'src/pages/_index.php';
require_once  ALT5PAY_DIR . 'src/admin/_index.php';